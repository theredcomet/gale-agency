"""
Signals:

Triggered when a new Domain or Component is created
"""
import logging
import time
from collections import namedtuple

import requests
import aiohttp
import asyncio
import async_timeout
from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientConnectorError
from django.db.models.signals import post_save
from django.dispatch import receiver

from .httputls import download_domain, download_component
from .models import Domain, Component

logger = logging.getLogger(__name__)

async def fetch(session, url):
    resp = dict()
    with async_timeout.timeout(100):
        try:
            headers = {'Accept-Encoding': 'gzip, deflate'}
            async with session.get(url, allow_redirects=True, headers=headers) as response:
                try:
                    text = await response.text()
                except UnicodeDecodeError: #PDF / other type of files
                    text = ""
                if not response.status == 200:
                    pass
                resp = {
                    "original_url": url,
                    "content": text,
                    "url": response.url,
                    "status": response.status}
        except ClientConnectorError:
            pass
    return resp

@receiver(post_save, sender=Domain)
def start_domain_parsing(sender, **kwargs):
    instance = kwargs.get('instance')
    if instance and kwargs.get('created'):
        download_domain.delay(instance)

@receiver(post_save, sender=Component)
def start_component_parsing(sender, **kwargs):
    instance = kwargs.get('instance')
    if instance and kwargs.get('created') & instance.depth <= instance.domain.depth:
        download_component.delay(instance)
