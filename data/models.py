from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Domain(models.Model):

    P_STATUS = (
        (0, "started"),
        (1, "processing"),
        (2, "done"),
    )

    title = models.CharField(max_length=250, unique=True)
    domain = models.URLField()
    created_on = models.DateField(auto_now_add=True)
    modified_on = models.DateField(auto_now=True)
    depth = models.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(11)],
        help_text="depth should be from 1 to 10")
    p_status = models.IntegerField(default=0, choices=P_STATUS)
    status = models.IntegerField(default=0)
    error_message = models.TextField(null=True)

    def __str__(self):
        return self.title


class Component(models.Model):
    TYPE = (
        (0, "url"),
        (1, "img"),
    )

    title = models.CharField(max_length=250, null=True)
    url = models.URLField(unique=True)
    created_on = models.DateField(auto_now_add=True)
    modified_on = models.DateField(auto_now=True)
    c_type = models.IntegerField(verbose_name="Component Type", choices=TYPE)
    depth = models.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(11)],
        help_text="depth should be from 1 to 10")
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)
    parent_component = models.ForeignKey(
        "Component",
        null=True,
        on_delete=models.CASCADE,
        limit_choices_to={"c_type": 0})
    status = models.IntegerField(default=0)
    error_message = models.TextField(null=True)

    def __str__(self):
        return self.url
