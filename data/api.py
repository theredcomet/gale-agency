from rest_framework import serializers, viewsets
from url_filter.integrations.drf import DjangoFilterBackend
from .models import Domain, Component


class DomainSerializer(serializers.HyperlinkedModelSerializer):
    p_status = serializers.CharField(source="get_p_status_display", read_only=True)

    class Meta:
        model = Domain
        fields = ('id', 'title', 'domain', 'depth', 'p_status')

    def get_p_status(self, obj):
        return obj.get_p_status_display()

class DomainViewSet(viewsets.ModelViewSet):
    queryset = Domain.objects.all()
    serializer_class = DomainSerializer


class ComponentSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Component
        fields = ('id', 'title', 'url', 'depth', 'status', 'domain')

    def get_p_status(self, obj):
        return obj.get_p_status_display()

class ComponentViewSet(viewsets.ModelViewSet):
    queryset = Component.objects.all()
    filter_backends = [DjangoFilterBackend]
    filter_fields = ('domain',)
    serializer_class = ComponentSerializer