axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'
var bootElement;


document.addEventListener("DOMContentLoaded", function () {

    bootElement = new Vue({
        el: '#app',
        theme: {
            primary: "#EF6C00",
            secondary: "#FFEBEE",
            accent: "#EF6C00",
            error: "#f44336",
            warning: "#ffeb3b",
            info: "#2196f3",
            success: "#4caf50",
            violet: "#674494",
        },
        data: {
            dialog: false,
            headers: [
                {
                    text: "Title",
                    align: "left",
                    value: "title"
                },
                { text: "Domain", value: "domain", align: "left" },
                { text: "Depth", value: "depth", align: "left" },
                { text: "Status", value: "p_status", align: "left" },
            ],
            desserts: [],
            editedIndex: -1,
            editedItem: {
                title: "",
                domain: "",
                depth: 1
            },
            defaultItem: {
                title: "",
                domain: "",
                depth: 1
            },
            selectedDomain: "",
            components: []
        },
        computed: {
            formTitle() {
                return this.editedIndex === -1 ? "New domain" : "Edit domain";
            }
        },

        watch: {
            dialog(val) {
                val || this.close();
            },
            selectedDomain(val) {
                if (val != "") {
                    axios
                        .get(`/api/component/?domain=${this.selectedDomain}`)
                        .then(
                            response => ((this.msg = "success"), (this.components = response.data))
                        )
                        .catch(error => (this.msg = error));
                }
                else {
                    this.components = []
                }
            },
        },

        created() {
            this.initialize();
            // this.getDomains();
        },

        methods: {
            initialize: function () {
                axios
                    .get(`/api/domain/`)
                    .then(
                        response => ((this.msg = "success"), (this.desserts = response.data))
                    )
                    .catch(error => (this.msg = error));
            },

            editItem(item) {
                this.editedIndex = this.desserts.indexOf(item);
                this.editedItem = Object.assign({}, item);
                this.dialog = true;
            },

            deleteItem(item) {
                const index = this.desserts.indexOf(item);
                axios
                    .delete(`/api/domain/${item.id}`)
                    .then(
                        response => ((this.initialize()))
                    )
                    .catch(error => (this.msg = error));
            },

            close() {
                this.dialog = false;
                setTimeout(() => {
                    this.editedItem = Object.assign({}, this.defaultItem);
                    this.editedIndex = -1;
                }, 300);
            },

            save() {
                let data = this.editedItem;
                axios({
                    method: "post",
                    url: `/api/domain/`,
                    "Content-Type": "raw",
                    data
                })
                    .then(response => (this.initialize(), this.close()))
                    .catch(function (error) {
                        console.log(error);
                    });
                // this.close();
            },
            setDomain(id) {
                this.selectedDomain = id
            }
        }
    });
});
function doc_keyUp(e) {
    if (e.keyCode == 27 && bootElement.dialog == true) {
        bootElement.dialog = false;
    }
}


document.addEventListener('keyup', doc_keyUp, false);

window.onscroll = function () {
    var scrollHeight, totalHeight;
    scrollHeight = document.body.scrollHeight;
    totalHeight = window.scrollY + window.innerHeight;

    if (totalHeight >= scrollHeight && bootElement.loading == false) {
        bootElement.getProduct()
    }
}