from django.contrib import admin

from .models import Component

class ComponentAdmin(admin.ModelAdmin):
    pass


admin.site.register(Component, ComponentAdmin)
