import re
from urllib.parse import urlparse
from bs4 import BeautifulSoup as BS
import requests
from django_rq import job
import logging
from .models import Component
logger = logging.getLogger(__name__)


def get_netloc(urlx):
    if "www." in urlx:
        urlx = urlx.replace("www.", "")
    if urlx.endswith("/"):
        urlx = urlx[:-1]
    url_schema = urlparse(urlx)
    netloc = url_schema.netloc
    return netloc


def disguise(urlx):
    """
    Reduces relative paths to absolute
    Ex:
        http://google.com/projects/../careers to
        http://google.com/careers
    """
    url_obj = urlparse(urlx)
    a = re.findall(r"(/\w*/+../)", url_obj.path)
    while a:
        for x in a:
            urlx = urlx.replace(x, "/")
            url_obj = urlparse(urlx)
            a = re.findall(r"(/\w*/+../)", url_obj.path)
    if urlx.endswith("/"):
        urlx = urlx[:-1]
    return urlx

def get_protocol(url):
    protocol = None
    if 'http://' in url:
        protocol = 'http://'
    elif 'https://' in url:
        protocol = 'https://'
    return protocol

def get_domain(starturl):
    starturl = starturl.lower()
    starturl = starturl.replace('www.', '')
    prefix = ['https://', 'http://']
    if starturl.endswith('/'):
        starturl = starturl[:-1]
    for pre in prefix:
        if starturl.startswith(pre):
            starturl = starturl.replace(pre, '')
    starturl = starturl.split('/')[0]
    return starturl

def modify_url(url):
    protocol = get_protocol(url)
    if protocol is not None and 'www.' not in url:
        url = url.replace(protocol, '{}www.'.format(protocol))
    else:
        url = None
    return url



def url_formatter(url, starturl=None):
    """
    Returns absolute url (concatenates with starturl)
    Start url is the Base URL
    """
    formatted_url = None
    if url is not None:
        url = url.strip()
        not_allowed = [
            '.pdf', '.jpg', '.jpeg', '.gif', 'mailto:', 'tel:',
            '.png', '.mp3', '.mp4', '.flv', '.svg', '.webm', '.webp',
            '.css', '.js', 'download', 'slideshare.net', 'javascript']
        check = [na for na in not_allowed if na in url]
        if not check:
            if starturl is None:
                domain = get_domain(url)
            else:
                domain = get_domain(starturl)
            starturl = 'http://{}'.format(domain)
            url = url.lower()
            if '#' in url:
                url = url.split('#')[0]
            if '?' in url:
                url = url.split('?')[0]
            if url.startswith('/'):
                url = '{}{}'.format(starturl, url)
            if not url.startswith('http://'):
                if not url.startswith('https://'):
                    if not url.startswith('www.'):
                        url = '{}/{}'.format(starturl, url)
            if url.endswith('/'):
                url = url[:-1]
            url2 = url
            if url2.startswith('http') or url2.startswith('https'):
                url = url.replace('www.', '')
                url_split = url2.split('/')
                url_split = [x for x in url_split if x != ""]
                if len(url_split) == len(list(set(url_split))):
                    formatted_url = url
            if formatted_url.startswith('http'):
                formatted_url = formatted_url.strip('http://')
            if formatted_url.startswith('https'):
                formatted_url = formatted_url.strip('https://')

    return formatted_url


def url_formatter_img(url, starturl=None):
    """
    Returns absolute url (concatenates with starturl)
    Start url is the Base URL
    """
    formatted_url = None
    if url is not None:
        url = url.strip()
        not_allowed = [
            '.pdf', '.gif', 'mailto:', 'tel:',
            '.png', '.mp3', '.mp4', '.flv', '.svg', '.webm',
            '.css', '.js', 'download', 'slideshare.net', 'javascript']
        check = [na for na in not_allowed if na in url]
        if not check:
            if starturl is None:
                domain = get_domain(url)
            else:
                domain = get_domain(starturl)
            starturl = 'http://{}'.format(domain)
            url = url.lower()
            if '#' in url:
                url = url.split('#')[0]
            if '?' in url:
                url = url.split('?')[0]
            if url.startswith('/'):
                url = '{}{}'.format(starturl, url)
            if not url.startswith('http://'):
                if not url.startswith('https://'):
                    if not url.startswith('www.'):
                        url = '{}/{}'.format(starturl, url)
            if url.endswith('/'):
                url = url[:-1]
            url2 = url
            if url2.startswith('http') or url2.startswith('https'):
                url = url.replace('www.', '')
                url_split = url2.split('/')
                url_split = [x for x in url_split if x != ""]
                if len(url_split) == len(list(set(url_split))):
                    formatted_url = url
            if formatted_url.startswith('http'):
                formatted_url = formatted_url.strip('http://')
            if formatted_url.startswith('https'):
                formatted_url = formatted_url.strip('https://')

    return formatted_url

def url_formatter_2(url, origin):
    """
    New method to clean the urls
    """
    not_allowed = [
        '.pdf', '.jpg', '.jpeg', '.gif', 'mailto:', 'tel:',
        '.png', '.mp3', '.mp4', '.flv', '.svg', '.webm', '.webp',
        '.css', '.js', 'download', 'slideshare.net', 'javascript']
    
    resp_url = None
    url = url.strip()
    starturl = None
    url_obj = urlparse(url)
    origin_obj = urlparse(origin)
    if origin_obj.scheme and origin_obj.netloc:
        starturl = "{}://{}".format(origin_obj.scheme, origin_obj.netloc)
    if not url_obj.netloc and not url_obj.path.startswith("/"):
        if origin_obj.path.endswith(".html") or origin_obj.path.endswith(".php"):
            d_origin = "/".join(origin_obj.path.split("/")[:-1])
            url = "{}://{}{}/{}".format(origin_obj.scheme, origin_obj.netloc, d_origin, url)
            url_obj = urlparse(url)
            url = url.split("#")[0]
            url = url.split("?")[0]
    else:
        if not url_obj.path.startswith("/"):
            url = "{}://{}/{}".format(url_obj.scheme, url_obj.netloc, url_obj.path)
        else:
            url = "{}://{}{}".format(url_obj.scheme, url_obj.netloc, url_obj.path)
    if "www." in url:
        url = url.replace("www.", "")

    check = [na for na in not_allowed if na in url]
    if not check:
        resp_url = url
    return resp_url
    

def get_links(data):
    starturl = data.get("original_url")
    starturl = starturl.__str__()
    soup = BS(data.get("content"), 'lxml')
    all_links = soup.find_all('a', href=True)
    links = [x.get("href") for x in all_links if x.get("href")]
    links = [url_formatter(url, starturl) for url in links]
    links = [x for x in links if x]
    return links

def get_links_media(data):
    starturl = data.get("original_url")
    starturl = starturl.__str__()
    soup = BS(data.get("content"), 'lxml')
    all_links = soup.find_all('img', src=True)
    links = [x.get("src") for x in all_links if x.get("href")]
    links = [url_formatter_img(url, starturl) for url in links]
    links = [x for x in links if x]
    return links


@job('high')
def download_domain(instance):
    url = instance.domain
    domain = get_domain(url)
    resp = requests.get(url)
    if resp.status_code == 200:
        data = {"original_url": url, "content": resp.content}
        links = get_links(data)
        links = [x for x in links if get_domain(x)==domain]
        links = list(set(links))
        print(links)
        for link in links:
            new_comp = Component(url=link, c_type=0, depth=1, domain=instance)
            new_comp.clean()
            try:
                new_comp.save()
            except Exception as e:
                logger.error("Component saving error: {}".format(e))

@job('high')
def download_component(instance):
    url = instance.url
    domain = get_domain(url)
    resp = requests.get('https://' + url)
    instance.status = resp.status_code
    instance.save()
    if resp.status_code == 200:
        data = {"original_url": url, "content": resp.content}
        links = get_links(data)
        links = [x for x in links if get_domain(x)==domain]
        links = list(set(links))
        # m_links = get_links_media(data)
        # m_links = [x for x in m_links if get_domain(x)==domain]
        # m_links = list(set(m_links))

        for link in links:
            new_comp = Component(url=link, c_type=0, depth=instance.depth+1, domain=instance.domain)
            new_comp.clean()
            try:
                new_comp.save()
            except Exception as e:
                logger.error("Component saving error: {}".format(e))

    # if Component.objects.filter(domain=instance.domain, status=0).count() == 0:
    #     instance.domain.p_status = 2
    #     instance.domain.save()