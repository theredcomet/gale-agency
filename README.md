# GALE

Project is rapid (prototype)-development for URL crawler.

-   User (authentication is not required yet) can add new domain:
-   using django signals, on domain Save (post_save) a function is called/triggered
-   called function will offload the job to unblock I/O to another function using django rq (uses redis server to manage tasks)
-   Dowloaded page will parsed for components (can be configured), in this project it is only href tags though parsing images is also written not called during
execution.
    
## Components

Components is a generic term used in this project instead of specific DOM elements such as href links or email IDS or images.

## Enhancements

*  This can be enhanced with multiple rq workers
*  Asynchronous webpage requests (code is present but not called as hosting requieres Python 3.4+ version)
*  Components Master table can be created to store details of DOM elements that should / can be parsed. Example, IMG, SVG, Vidoes, JS tags, iframe links, text parts.


## Hosting

### system requirements

-   redis-server
-   python3.x
-   nginx/apache

### Python requirements

These are listed in requirements file.

-   django 2.x
-   djangorestframework
-   django_url_filter
-   rqworker
-   gunicorn (to host during production)
